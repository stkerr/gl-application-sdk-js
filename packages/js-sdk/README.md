# GitLab Application SDK - JS Core

This SDK is for usage of GitLab Application Services with vanilla Javascript.

## Development guidelines

* `yarn build` builds the npm packages and the classic browser library
* `yarn test` builds the packages and runs jest tests
* `yarn clean` cleans the dist folder
* `npm publish --public` to publish a newly built package. You need to run `npm login` with your personal npm login before.
