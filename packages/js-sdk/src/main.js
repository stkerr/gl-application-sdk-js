import { jitsuClient } from '@jitsu/sdk-js';
import { doNotTrackEnabled } from './utils/doNotTrack';

const hasDNT = doNotTrackEnabled();

export function glClientSDK(opts) {
  return new GitLabClientSDK(opts);
}

class GitLabClientSDK {
  #hasDNT = doNotTrackEnabled();
  #jitsu = null;

  constructor(opts) {
    if (this.#hasDNT) return;

    if (!opts?.applicationId) {
      console.warn('GitLab: No applicationId was provided');
      return;
    }

    if (!opts?.host) {
      console.warn('GitLab: No host was provided');
      return;
    }

    //Initialize Jitsu Client with options
    this.#jitsu = jitsuClient({
      key: opts.applicationId,
      tracking_host: opts.host,
    });
  }

  track(eventId, eventOpts) {
    if (this.#hasDNT) return;
    this.#jitsu.track(eventId, { ...eventOpts });
  }

  page(eventOpts) {
    this.track('pageview', { ...eventOpts });
  }

  identify(userId, userAttributes) {
    if (this.#hasDNT) return;
    this.#jitsu.id({
      id: userId,
      ...userAttributes,
    });
  }
}
