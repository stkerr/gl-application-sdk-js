export function doNotTrackEnabled() {
  // Based on checks defined in https://github.com/DavidWells/analytics/blob/master/packages/analytics-plugin-do-not-track/src/index.js
  // Not running a browser
  if (typeof window === 'undefined') return false;

  const { doNotTrack, navigator } = window;
  const dntSetting =
    doNotTrack || navigator.doNotTrack || navigator.msDoNotTrack || msTrackingProtection();

  if (!dntSetting) {
    return false;
  }

  if (
    dntSetting === true ||
    dntSetting === 1 ||
    dntSetting === 'yes' ||
    (typeof dntSetting === 'string' && dnt.charAt(0) === '1')
  ) {
    return true;
  }

  return false;
}

function msTrackingProtection() {
  const { external } = window;
  return (
    typeof external !== 'undefined' &&
    'msTrackingProtectionEnabled' in external &&
    typeof external.msTrackingProtectionEnabled === 'function' &&
    window.external.msTrackingProtectionEnabled()
  );
}
