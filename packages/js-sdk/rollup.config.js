export default [
  {
    input: 'src/main.js',
    output: {
      file: 'dist/npm/gl-sdk.js',
      format: 'cjs',
    },
  },
];
