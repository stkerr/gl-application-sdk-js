import { doNotTrackEnabled } from '../../src/utils/doNotTrack';

describe('running in browser', () => {
  it('returns false by default', () => {
    expect(doNotTrackEnabled()).toBe(false);
  });

  it('returns true if window.doNotTrack is set', () => {
    Object.defineProperty(window, 'doNotTrack', {
      writable: true,
      value: true,
    });
    expect(doNotTrackEnabled()).toBe(true);
    window.doNotTrack = undefined;
  });

  it('returns true if navigator.doNotTrack is set', () => {
    Object.defineProperty(navigator, 'doNotTrack', {
      writable: true,
      value: true,
    });
    expect(doNotTrackEnabled()).toBe(true);
    navigator.doNotTrack = undefined;
  });

  it('returns true if navigator.msDoNotTrack is set', () => {
    Object.defineProperty(navigator, 'msDoNotTrack', {
      writable: true,
      value: true,
    });
    expect(doNotTrackEnabled()).toBe(true);
    navigator.msDoNotTrack = undefined;
  });
});
