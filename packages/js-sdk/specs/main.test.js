import { doNotTrackEnabled } from '../src/utils/doNotTrack';
import { glClientSDK } from '../src/main';

jest.mock('../src/utils/doNotTrack', () => ({
  doNotTrackEnabled: jest.fn(),
}));

const mockTrack = jest.fn();
const mockId = jest.fn();

jest.mock('@jitsu/sdk-js', () => ({
  jitsuClient: jest.fn().mockImplementation(() => ({ track: mockTrack, id: mockId })),
}));

describe('GlClient', () => {
  let glClient;

  beforeAll(() => {
    Object.defineProperty(document, 'cookie', {
      get: jest.fn().mockImplementation(() => {
        return '';
      }),
      set: jest.fn().mockImplementation(() => {}),
    });
  });

  describe('with DNT enabled', () => {
    beforeEach(() => {
      doNotTrackEnabled.mockReturnValue(true);

      glClient = glClientSDK({
        applicationId: '123',
        host: 'collector.gitlab.com',
      });
    });

    it('basic setup initialises GlClientSDK', () => {
      expect(glClient).toBeDefined();
    });

    describe('track', () => {
      it('does not call Jitsu', () => {
        glClient.track('testEvent');

        expect(mockTrack).not.toHaveBeenCalled();
      });
    });

    describe('page', () => {
      it('does not call Jitsu', () => {
        glClient.page();

        expect(mockTrack).not.toHaveBeenCalled();
      });
    });

    describe('identify', () => {
      it('does not call Jitsu', () => {
        glClient.identify('1');

        expect(mockId).not.toHaveBeenCalled();
      });
    });
  });

  describe('without DNT enabled', () => {
    beforeEach(() => {
      doNotTrackEnabled.mockReturnValue(false);

      glClient = glClientSDK({
        applicationId: '123',
        host: 'collector.gitlab.com',
      });
    });

    it('basic setup initialises GlClientSDK', () => {
      expect(glClient).toBeDefined();
    });

    describe('track', () => {
      it('call will send off Jitsu Call', () => {
        glClient.track('testEvent');
        expect(mockTrack).toHaveBeenCalledWith('testEvent', {});
      });
    });

    describe('page', () => {
      it('call will send off Jitsu Track Call', () => {
        glClient.page();
        expect(mockTrack).toHaveBeenCalledWith('pageview', {});
      });
    });

    describe('identify', () => {
      it('call will send off Jitsu id Call', () => {
        glClient.identify('1');
        expect(mockId).toHaveBeenCalledWith({ id: '1' });
      });
    });
  });
});

describe('configuration checks', () => {
  beforeAll(() => {
    jest.spyOn(console, 'warn').mockImplementation(() => {});
  });

  afterAll(() => {
    console.warn.mockRestore();
  });

  afterEach(() => {
    console.warn.mockClear();
  });

  it('Creates console warning if no tracking ID was provided', () => {
    glClientSDK();
    expect(console.warn).toHaveBeenCalled();
    expect(console.warn.mock.calls[0][0]).toContain('GitLab: No applicationId was provided');
  });

  it('Creates console warning if no host was provided', () => {
    glClientSDK({
      applicationId: '123',
    });
    expect(console.warn).toHaveBeenCalled();
    expect(console.warn.mock.calls[0][0]).toContain('GitLab: No host was provided');
  });
});
