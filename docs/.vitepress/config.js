export default {
  title: 'GitLab Application SDK',
  outDir: '../public',
  base: '/analytics-section/product-analytics/gl-application-sdk-js/',
  layout: 'doc',
  appearance: true,
};
