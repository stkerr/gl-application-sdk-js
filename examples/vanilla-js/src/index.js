import { glClientSDK } from '../../../packages/js-sdk/dist/npm/gl-sdk';

const gitlabSDK = glClientSDK({
  applicationId: '123',
  host: 'test.gl.com',
});

gitlabSDK.page();

document.getElementById('testClickBtn').onclick = function () {
  gitlabSDK.track('click', { elementId: 'testClickBtn' });
};

document.getElementById('testIdBtn').onclick = function () {
  gitlabSDK.identify('123', { userType: 'premium' });
};
