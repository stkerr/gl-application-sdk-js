const fs = require('fs');
const path = require('path');

fs.mkdirSync('public/examples', { recursive: true });

// We copy all dist builds from the examples to our output Folder
fs.readdirSync('examples').forEach((subFolder) => {
  fs.cpSync('examples/' + subFolder + '/dist', 'public/examples/' + subFolder, { recursive: true });
});
